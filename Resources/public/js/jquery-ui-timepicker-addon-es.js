$.timepicker.regional['es'] = {
	timeOnlyTitle: 'Solo hora',
	timeText: 'Hora',
	hourText: 'hora',
	minuteText: 'minutos',
	secondText: 'segundos',
	millisecText: 'mili segundos',
	timezoneText: 'zona horaria',
	currentText: 'Ahora',
	closeText: 'Cerrar',
	timeFormat: 'HH:mm',
	amNames: ['AM', 'A'],
	pmNames: ['PM', 'P'],
	isRTL: false
};
$.timepicker.setDefaults($.timepicker.regional['es']);