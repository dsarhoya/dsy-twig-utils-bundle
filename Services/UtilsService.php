<?php

namespace dsarhoya\DSYTwigUtilsBundle\Services;

/**
 * Utils Service.
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class UtilsService
{
    /**
     * @param \DateTime $date
     * @param bool|null $long
     */
    public function toWordsFilter($date, $long = true)
    {
        $now = new \DateTime('now');

        return $this->dateDifferenceToWordsFunction($now, $date, $long);
    }

    /**
     * @param \DateTime $date
     * @param bool|null $long
     */
    public function dateToWordsFunction($date, $long = false)
    {
        $now = new \DateTime('now');

        return $this->dateDifferenceToWordsFunction($now, $date, $long);
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @param bool|null $long
     *
     * @return string
     */
    public function dateDifferenceToWordsFunction($from, $to, $long = true)
    {
        $interval = $from->diff($to);

        if ($interval->y >= 1) {
            return  $this->pluralize($interval->y, 'año').($interval->m >= 1 && $long ? ', '.$this->pluralize($interval->m, 'mes') : '');
        }
        if ($interval->m >= 1) {
            return  $this->pluralize($interval->m, 'mes').($interval->d >= 1 && $long ? ', '.$this->pluralize($interval->d, 'día') : '');
        }
        if ($interval->d >= 1) {
            return  $this->pluralize($interval->d, 'día').($interval->h >= 1 && $long ? ', '.$this->pluralize($interval->h, 'hora') : '');
        }
        if ($interval->h >= 1) {
            return  $this->pluralize($interval->h, 'hora').($interval->i >= 1 && $long ? ', '.$this->pluralize($interval->i, 'minuto') : '');
        }
        if ($interval->i >= 1) {
            return  $this->pluralize($interval->i, 'minuto');
        }

        return $this->pluralize($interval->s, 'segundo');
    }

    /**
     * @param int    $count
     * @param string $text
     *
     * @return string
     */
    private function pluralize($count, $text)
    {
        return $count.((1 == $count) ? (" $text") : (' '.$this->pluralizeUnit($text)));
    }

    /**
     * @param string $unit
     *
     * @return string
     */
    private function pluralizeUnit($unit)
    {
        switch ($unit) {
            case 'año': return 'años';
            case 'mes': return 'meses';
            case 'día': return 'días';
            case 'hora': return 'horas';
            case 'minuto': return 'minutos';
            case 'segundo': return 'segundos';
        }
    }

    /**
     * @param float|int $number
     * @param string    $format
     * @param int|null  $decimals
     *
     * @return string
     */
    public function currencyFilter($number, $format, $decimals = null)
    {
        switch (strtolower($format)) {
            case 'uf':
                    $price = number_format($number, (null !== ($decimals) ? $decimals : 2), ',', '.');
                    $price = $price.' UF';
                break;
            case 'clp':
                    $price = number_format($number, (null !== ($decimals) ? $decimals : 0), ',', '.');
                    $price = '$ '.$price;
                break;
            case 'usd':
                    $price = number_format($number, (null !== ($decimals) ? $decimals : 2), '.', ',');
                    $price = 'US$ '.$price;
                break;
            case 'eur':
                    $price = number_format($number, (null !== ($decimals) ? $decimals : 2), ',', '.');
                    $price = '€ '.$price;
                break;
            default:
                $price = number_format($number, (null !== ($decimals) ? $decimals : 0), ',', '.');
                $price = $price;
        }

        return $price;
    }
}
