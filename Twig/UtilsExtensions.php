<?php

namespace dsarhoya\DSYTwigUtilsBundle\Twig;

use dsarhoya\DSYTwigUtilsBundle\Services\UtilsService;
use Twig\TwigFunction;
use Twig\TwigFilter;

/**
 * Description of UtilsExtensions.
 *
 * @author matias
 */
class UtilsExtensions extends \Twig_Extension
{
    /**
     * @var UtilsService
     */
    private $utilsSrv;

    /**
     * Constructor.
     *
     * @param UtilsService $utilsSrv
     */
    public function __construct(UtilsService $utilsSrv)
    {
        $this->utilsSrv = $utilsSrv;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dsy-twig-utils';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new TwigFunction('dateDifferenceToWords', array($this->utilsSrv, 'dateDifferenceToWordsFunction')),
            new TwigFunction('dateToWords', array($this->utilsSrv, 'dateToWordsFunction')),
            new TwigFunction('ini_get', array($this, 'ini_getFunction')),
            new TwigFunction('staticCall', array($this, 'staticCallFunction')),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return array(
            new TwigFilter('toWords', array($this->utilsSrv, 'toWordsFilter')),
            new TwigFilter('currency', array($this->utilsSrv, 'currencyFilter')),
        );
    }

    public function ini_getFunction($php_ini_variable)
    {
        return ini_get($php_ini_variable);
    }

    public function staticCallFunction($class, $function, $args = array())
    {
        if (class_exists($class) && method_exists($class, $function)) {
            return call_user_func_array(array($class, $function), $args);
        }

        return null;
    }
}
